-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mastodon
-- -----------------------------------------------------

USE `mastodon` ;

-- -----------------------------------------------------
-- Table `User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `User` ;

CREATE TABLE IF NOT EXISTS `User` (
  `userid` INT NOT NULL AUTO_INCREMENT,
  `nickname` VARCHAR(16) NULL,
  `emailAddress` VARCHAR(64) NULL,
  `incepDate` DATETIME NULL,
  PRIMARY KEY (`userid`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `nickname_UNIQUE` ON `User` (`nickname` ASC);


-- -----------------------------------------------------
-- Table `Privilege`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Privilege` ;

CREATE TABLE IF NOT EXISTS `Privilege` (
  `privilegeid` CHAR(8) NOT NULL,
  `description` VARCHAR(48) NULL,
  PRIMARY KEY (`privilegeid`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `UserPrivilege`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `UserPrivilege` ;

CREATE TABLE IF NOT EXISTS `UserPrivilege` (
  `userid` INT NOT NULL,
  `privilegeid` CHAR(8) NOT NULL,
  PRIMARY KEY (`userid`, `privilegeid`),
  CONSTRAINT `fk_userPrivilege_User`
    FOREIGN KEY (`userid`)
    REFERENCES `User` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_userPrivilege_privilege1`
    FOREIGN KEY (`privilegeid`)
    REFERENCES `Privilege` (`privilegeid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_userPrivilege_privilege1_idx` ON `UserPrivilege` (`privilegeid` ASC);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
