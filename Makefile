
SQL_FILES=$(shell ls sql/*.sql)

sql/mastodonDB.made: ${SQL_FILES}
	for file in ${SQL_FILES} ; do \
		echo EXEC $$file ; \
		mysql --host=127.0.0.1 --port=3306 --user=pachyderm --password=mammutine10K < $$file ; \
	done
	# mysql --host=127.0.0.1 --port=3306 --user=pachyderm --password=mammutine10K < sql/mastodon-schema.sql
	date > sql/mastodonDB.made
